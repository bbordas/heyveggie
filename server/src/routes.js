const AuthenticationController = require('./controllers/AuthenticationController')
const AuthenticationControllerPolicy = require('./policies/AuthenticationControllerPolicy')
const isAuthenticated = require('./policies/isAuthenticated')
const UsersController = require('./controllers/UsersController')
const ArticlesController = require('./controllers/ArticlesController')
const ArticleCommentController = require('./controllers/ArticleCommentController')
const RecipesController = require('./controllers/RecipesController')
const RecipeCommentController = require('./controllers/RecipeCommentController')
const CuisinesController = require('./controllers/CuisinesController')
const IngredientsController = require('./controllers/IngredientsController')
const FoodGroupController = require('./controllers/FoodGroupController')
const FoodController = require('./controllers/FoodController')
const WeightController = require('./controllers/WeightController')
const NutrientController = require('./controllers/NutrientController')
const NutritionController = require('./controllers/NutritionController')

module.exports = (app) => {

// ************************** REGISTER AND LOGIN AND USER ******************************
  app.post('/register',
    AuthenticationControllerPolicy.register, 
    AuthenticationController.register) 
  app.post('/login',
    AuthenticationController.login)
  app.get('/users',
    isAuthenticated,
    UsersController.index)
  app.get('/users/:userId',
    isAuthenticated,
    UsersController.show)
  app.put('/users/:userId',
    isAuthenticated,
    UsersController.put)
  app.delete('/users/:userId',
    isAuthenticated,  
    UsersController.remove)
  
// ************************** RECIPES ******************************
  app.get('/recipes',
    RecipesController.index)
  app.get('/recipes/:recipeId',
    RecipesController.show)
  app.post('/recipes',
    isAuthenticated,
    RecipesController.post)
  app.put('/recipes/:recipeId',
    isAuthenticated,
    RecipesController.put)
  app.delete('/recipes/:recipeId',
    isAuthenticated,  
    RecipesController.remove)

// ************************** ARTICLES ******************************
  app.get('/articles',
    ArticlesController.index)
  app.get('/articles/:articleId',
    ArticlesController.show)
  app.post('/articles',
    isAuthenticated,
    ArticlesController.post)
  app.put('/articles/:articleId',
    isAuthenticated,
    ArticlesController.put)
  app.delete('/articles/:articleId',
    isAuthenticated,  
    ArticlesController.remove)

// ************************** ARTICLE COMMENTS ******************************
  app.get('/articlecomments',
    ArticleCommentController.index)
  app.get('/articlecomments/:articlecommentId',
    ArticleCommentController.show)
  app.post('/articlecomments',
    isAuthenticated,
    ArticleCommentController.post)
  app.put('/articlecomments/:articlecommentId',
    isAuthenticated,
    ArticleCommentController.put)
  app.delete('/articlecomments/:articlecommentId',
    isAuthenticated,  
    ArticleCommentController.remove)

// ************************** RECIPE COMMENTS ******************************

  app.get('/recipecomments',
    RecipeCommentController.index)
  app.get('/recipecomments/:recipecommentId',
    RecipeCommentController.show)
  app.post('/recipecomments',
    isAuthenticated,
    RecipeCommentController.post)
  app.put('/recipecomments/:recipecommentId',
    isAuthenticated,
    RecipeCommentController.put)
  app.delete('/recipecomments/:recipecommentId',
    isAuthenticated,  
    RecipeCommentController.remove)

// ************************** INGREDIENTS ******************************
  app.get('/ingredients',
    IngredientsController.index)
  app.get('/ingredients/:ingredientId',
    IngredientsController.show)
  app.post('/ingredients',
    isAuthenticated,
    IngredientsController.post)
  app.put('/ingredients/:ingredientId',
    isAuthenticated,  
    IngredientsController.remove)
  app.delete('/ingredients/:ingredientId',
    isAuthenticated,  
    IngredientsController.remove)

// ************************** CUISINE ******************************
  app.get('/cuisine',
    CuisinesController.index)
  app.get('/cuisine/:cuisineId',
    CuisinesController.show)
  app.post('/cuisine',
    isAuthenticated,
    CuisinesController.post)

// ************************** FOOD ******************************
  app.get('/food',
    FoodController.index)
  app.get('/food/:foodId',
    FoodController.show)

// ************************** WEIGHT ******************************
  app.get('/weight',
    WeightController.index)
  app.get('/weight/:weightId',
    WeightController.show)

// ************************** NUTRITION ******************************
app.get('/nutrition',
NutritionController.index)
app.get('/nutrition/:nutritionId',
NutritionController.show)

// ************************** NUTRIENT ******************************
app.get('/nutrient',
NutrientController.index)
app.get('/nutrient/:nutrientId',
NutrientController.show)

// ************************** FOOD GROUP ******************************
app.get('/foodgroup',
FoodGroupController.index)
app.get('/foodgroup/:foodgroupId',
FoodGroupController.show)


}
