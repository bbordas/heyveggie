const {Nutrient} = require('../models')
module.exports = {
  async index (req, res) {
    try {
      let nutrients = null
      const search = req.query.search
      if (search) {
        nutrients = await Nutrient.findAll({
          where: {
            $or: [
              'name'
            ].map(key => ({
              [key]: {
                $like: `%${search}%`
              }
            }))
          }
        })
      } else {
        nutrients = await Nutrient.findAll({
          limit: 20
        })
      }
      res.send(nutrients)
    } catch (err) {
      res.status(500).send({
        error: 'An error has occured trying to fetch nutrition.'
      })
    }
  },
  async show (req, res) {
    try {
      const nutrient = await Nutrient.findById(req.params.id)
      res.send(nutrient)
    } catch (err) {
      res.status(500).send({
        error: 'An error has occured trying to fetch the nutrient.'
      })
    }
  }
}
