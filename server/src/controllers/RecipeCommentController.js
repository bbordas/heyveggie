const {RecipeComment} = require('../models')
module.exports = {
  async index (req, res) {
    try {
      let recipeComments = null
      const search = req.query.search
      if (search) {
        recipeComments = await RecipeComment.findAll({
          where: {
            $or: [
              'email', 'text'
            ].map(key => ({
              [key]: {
                $like: `%${search}%`
              }
            }))
          }
        })
      } else {
        recipeComments = await RecipeComment.findAll({
          limit: 20
        })
      }
      res.send(recipeComments)
    } catch (err) {
      res.status(500).send({
        error: 'An error has occured trying to fetch recipeComments.'
      })
    }
  },
  async show (req, res) {
    try {
      const recipeComment = await RecipeComment.findById(req.params.id)
      res.send(recipeComment)
    } catch (err) {
      res.status(500).send({
        error: 'An error has occured trying to fetch the recipeComment.'
      })
    }
  },
  async post (req, res) {
    try {
      const userId = req.user.id
      const recipeComment = await RecipeComment.create(req.body)
      console.log(userId)
      res.send(recipeComment)
    } catch (err) {
      res.status(500).send({
        error: 'An error has occured trying to create the recipeComment.'
      })
    }
  },
  async put (req, res) {
    try {
      await RecipeComment.update(req.body, {
        where: {
          id: req.params.id
        }
      })
      res.send(req.body)
    } catch (err) {
      res.status(500).send({
        error: 'an error has occured trying to update the recipeComment'
      })
    }
  },
  async remove (req, res) {
    try {
      const {id} = req.params
      const recipeComment = await RecipeComment.findOne({
        where: {
          id: id
        }
      })
      if (!recipeComment) {
        return res.status(403).send({
          error: 'you do not have access to this recipeComment'
        })
      }
      await recipeComment.destroy()
      res.send(recipeComment)
    } catch (err) {
      res.status(500).send({
        error: 'an error has occured trying to delete the recipeComment'
      })
    }
  }
}
