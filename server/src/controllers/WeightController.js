const {weight} = require('../models')
module.exports = {
  async index (req, res) {
    try {
      let weights = null
      const search = req.query.search
      if (search) {
        weights = await weight.findAll({
          where: {
            $or: [
              'description', 'food_id'
            ].map(key => ({
              [key]: {
                $like: `%${search}%`
              }
            }))
          }
        })
      } else {
        weights = await weight.findAll()
      }
      res.send(weights)
    } catch (err) {
      res.status(500).send({
        error: 'An error has occured trying to fetch weights.'
      })
    }
  },
  async show (req, res) {
    try {
      const weight = await weight.findById(req.params.food_id + req.params.sequence_num)
      res.send(weight)
    } catch (err) {
      res.status(500).send({
        error: 'An error has occured trying to fetch the weight.'
      })
    }
  }
}
