const {Nutrition} = require('../models')
module.exports = {
  async index (req, res) {
    try {
      let nutritions = null
      const search = req.query.search
      if (search) {
        nutritions = await Nutrition.findAll({
          where: {
            $or: [
              'nutrient_id', 'food_id'
            ].map(key => ({
              [key]: {
                $like: `%${search}%`
              }
            }))
          }
        })
      } else {
        nutritions = await Nutrition.findAll({
          limit: 20
        })
      }
      res.send(nutritions)
    } catch (err) {
      res.status(500).send({
        error: 'An error has occured trying to fetch nutritions.'
      })
    }
  },
  async show (req, res) {
    try {
      const nutrition = await Nutrition.findById(req.params.food_id + req.params.nutrient_id)
      res.send(nutrition)
    } catch (err) {
      res.status(500).send({
        error: 'An error has occured trying to fetch the nutrition.'
      })
    }
  }
}
