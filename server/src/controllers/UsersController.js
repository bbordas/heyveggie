const {User} = require('../models')

module.exports = {
  async index (req, res) {
    try {
      const userId = req.user.id
      let users = null
      console.log(userId)
      const search = req.query.search
      if (search) {
        users = await User.findAll({
          where: {
            $or: [
              'username', 'role', 'email'
            ].map(key => ({
              [key]: {
                $like: `%${search}%`
              }
            }))
          }
        })
      } else {
        users = await User.findAll({
          limit: 20
        })
      }
      res.send(users)
    } catch (err) {
      res.status(500).send({
        error: 'An error has occured trying to fetch the users.'
      })
    }
  },
  async show (req, res) {
    try {
      const userId = req.user.id
      const user = await User.findById(req.params.userId)
      res.send(user)
      console.log(userId)
    } catch (err) {
      res.status(500).send({
        error: 'An error has occured trying to fetch the user.'
      })
    }
  },
  async put (req, res) {
    try {
      await User.update(req.body, {
        where: {
          id: req.params.userId
        }
      })
      res.send(req.body)
    } catch (err) {
      res.status(500).send({
        error: 'an error has occured trying to update the user'
      })
    }
  },
  async remove (req, res) {
    try {
      const userId = req.user.id
      const user = await User.findOne({
        where: {
          id: userId
        }
      })
      if (!user) {
        return res.status(403).send({
          error: 'you do not have access to this user'
        })
      }
      await user.destroy()
      res.send(user)
    } catch (err) {
      res.status(500).send({
        error: 'an error has occured trying to delete the user'
      })
    }
  }
}
