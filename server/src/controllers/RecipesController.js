const {Recipe, User, RecipeComment} = require('../models')

module.exports = {
  async index (req, res) {
    try {
      let recipes = null
      const search = req.query.search
      if (search) {
        recipes = await Recipe.findAll({
          where: {
            $or: [
              'title', 'description', 'cuisine', 'chef'
            ].map(key => ({
              [key]: {
                $like: `%${search}%`
              }
            })),
            include: [
              { model: User, where: { id: req.params.chef } },
              { model: RecipeComment } 
            ]
          }
        })
      } else {
        recipes = await Recipe.findAll({
          include: [RecipeComment, User],
          limit: 100
        })
      }
      res.status(200).send(recipes)
      
    } catch (err) {
      res.status(500).send({
        error: 'An error has occured trying to fetch recipes.'
      })
    }
  },
  async show (req, res) {
    try {
      const recipe = await Recipe.findById({
        include: [
          { model: User, where: { id: req.params.chef } },
          { model: RecipeComment } 
        ]},
        req.params.recipeId)
      res.send(recipe)
    } catch (err) {
      res.status(500).send({
        error: 'An error has occured trying to fetch the recipe.'
      })
    }
  },
  async post (req, res) {
    try {
      console.log(req.body)
      const recipe = await Recipe.create(req.body)
      res.send(recipe)
    } catch (err) {
      res.status(500).send({
        error: 'An error has occured trying to create recipe.'
      })
    }
  },
  async put (req, res) {
    try {
      await Recipe.update(req.body, {
        where: {
          id: req.params.recipeId
        }
      })
      res.send(req.body)
    } catch (err) {
      res.status(500).send({
        error: 'an error has occured trying to update the recipe'
      })
    }
  },
  async remove (req, res) {
    try {
      const {recipeId} = req.params
      const recipe = await Recipe.findOne({
        where: {
          id: recipeId
        }
      })
      if (!recipe) {
        return res.status(403).send({
          error: 'you do not have access to this recipe'
        })
      }
      await recipe.destroy()
      res.send(recipe)
    } catch (err) {
      res.status(500).send({
        error: 'an error has occured trying to delete the recipe'
      })
    }
  }
}
