const {CookBook} = require('../models')

module.exports = {
  async index (req, res) {
    try {
      const {recipeId, userId} = req.query
        const cookbook = await CookBook.findOne({
          where: {
            RecipeId: recipeId,
            UserId: userId
          }
        })
      res.send(cookbook)
    } catch (err) {
      res.status(500).send({
        error: 'An error has occured trying to fetch the cookBook.'
      })
    }
  }
}
