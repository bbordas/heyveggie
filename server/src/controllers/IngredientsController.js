const {Ingredient} = require('../models')
module.exports = {
  async index (req, res) {
    try {
      let ingredients = null
      const search = req.query.search
      if (search) {
        ingredients = await Ingredient.findAll({
          where: {
            $or: [
              'food_id', 'recipe_id'
            ].map(key => ({
              [key]: {
                $like: `%${search}%`
              }
            }))
          }
        })
      } else {
        ingredients = await Ingredient.findAll({
          limit: 20
        })
      }
      res.send(ingredients)
    } catch (err) {
      res.status(500).send({
        error: 'An error has occured trying to fetch ingredients.'
      })
    }
  },
  async show (req, res) {
    try {
      const ingredient = await Ingredient.findById(req.params.id)
      res.send(ingredient)
    } catch (err) {
      res.status(500).send({
        error: 'An error has occured trying to fetch the ingredient.'
      })
    }
  },
  async post (req, res) {
    try {
      const userId = req.user.id
      const ingredient = await Ingredient.create(req.body)
      console.log(userId)
      res.send(ingredient)
    } catch (err) {
      res.status(500).send({
        error: 'An error has occured trying to create the ingredient.'
      })
    }
  },
  async put (req, res) {
    try {
      await Ingredient.update(req.body, {
        where: {
          id: req.params.ingredientId
        }
      })
      res.send(req.body)
    } catch (err) {
      res.status(500).send({
        error: 'an error has occured trying to update the ingredient'
      })
    }
  },
  async remove (req, res) {
    try {
      const {ingredientId} = req.params
      const ingredient = await Ingredient.findOne({
        where: {
          id: ingredientId
        }
      })
      if (!ingredient) {
        return res.status(403).send({
          error: 'you do not have access to this ingredient'
        })
      }
      await ingredient.destroy()
      res.send(ingredient)
    } catch (err) {
      res.status(500).send({
        error: 'an error has occured trying to delete the ingredient'
      })
    }
  }
}
