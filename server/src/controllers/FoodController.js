const {food} = require('../models')
module.exports = {
  async index (req, res) {
    try {
      let foods = null
      const search = req.query.search
      if (search) {
        foods = await food.findAll({
          where: {
            $or: [
              'long_desc', 'short_desc'
            ].map(key => ({
              [key]: {
                $like: `%${search}%`
              }
            }))
          }
        })
      } else {
        foods = await food.findAll()
      }
      res.send(foods)
    } catch (err) {
      res.status(500).send({
        error: 'An error has occured trying to fetch foods.'
      })
    }
  },
  async show (req, res) {
    try {
      const food = await food.findById(req.params.foodId)
      res.send(food)
    } catch (err) {
      res.status(500).send({
        error: 'An error has occured trying to fetch the food.'
      })
    }
  }
}
