const {Article} = require('../models')
module.exports = {
  async index (req, res) {
    try {
      let articles = null
      const search = req.query.search
      if (search) {
        articles = await Article.findAll({
          where: {
            $or: [
              'title', 'author', 'smallText'
            ].map(key => ({
              [key]: {
                $like: `%${search}%`
              }
            }))
          }
        })
      } else {
        articles = await Article.findAll({
          limit: 100
        })
      }
      res.status(200).send(articles)
    } catch (err) {
      res.status(500).send({
        error: 'An error has occured trying to fetch the articles.'
      })
    }
  },
  async show (req, res) {
    try {
      const article = await Article.findById(req.params.articleId)
      res.send(article)
    } catch (err) {
      res.status(500).send({
        error: 'An error has occured trying to fetch the article.'
      })
    }
  },
  async post (req, res) {
    try {
      console.log(req.body)
      const article = await Article.create(req.body)
      res.send(article)
    } catch (err) {
      res.status(500).send({
        error: 'An error has occured trying to create the article.'
      })
    }
  },
  async put (req, res) {
    try {
      await Article.update(req.body, {
        where: {
          id: req.params.articleId
        }
      })
      res.send(req.body)
    } catch (err) {
      res.status(500).send({
        error: 'an error has occured trying to update the article'
      })
    }
  },
  async remove (req, res) {
    try {
      const {articleId} = req.params
      const article = await Article.findOne({
        where: {
          id: articleId
        }
      })
      if (!article) {
        return res.status(403).send({
          error: 'you do not have access to this article'
        })
      }
      await article.destroy()
      res.send(article)
    } catch (err) {
      res.status(500).send({
        error: 'an error has occured trying to delete the article'
      })
    }
  }
}
