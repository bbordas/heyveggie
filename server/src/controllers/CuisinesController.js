const {Cuisines} = require('../models')
module.exports = {
  async index (req, res) {
    try {
      let cuisines = null
      const search = req.query.search
      if (search) {
        cuisines = await Cuisines.findAll({
          where: {
            $or: [
              'title', 'author', 'smallText'
            ].map(key => ({
              [key]: {
                $like: `%${search}%`
              }
            }))
          }
        })
      } else {
        cuisines = await Cuisines.findAll()
      }
      res.send(cuisines)
    } catch (err) {
      res.status(500).send({
        error: 'An error has occured trying to fetch the cuisines.'
      })
    }
  },
  async show (req, res) {
    try {
      const cuisine = await Cuisines.findById(req.params.cuisineId)
      res.send(cuisine)
    } catch (err) {
      res.status(500).send({
        error: 'An error has occured trying to fetch the cuisine.'
      })
    }
  },
  async post (req, res) {
    try {
      const userId = req.user.id
      const cuisine = await Cuisines.create(req.body)
      console.log(userId)
      res.send(cuisine)
    } catch (err) {
      res.status(500).send({
        error: 'An error has occured trying to create the cuisine.'
      })
    }
  }
}
