const {ArticleComment} = require('../models')
module.exports = {
  async index (req, res) {
    try {
      let articleComments = null
      const search = req.query.search
      if (search) {
        articleComments = await ArticleComment.findAll({
          where: {
            $or: [
              'email', 'comment'
            ].map(key => ({
              [key]: {
                $like: `%${search}%`
              }
            }))
          }
        })
      } else {
        articleComments = await ArticleComment.findAll({
          limit: 50
        })
      }
      res.send(articleComments)
    } catch (err) {
      res.status(500).send({
        error: 'An error has occured trying to fetch the articleComments.'
      })
    }
  },
  async show (req, res) {
    try {
      const articleComment = await ArticleComment.findById(req.params.id)
      res.send(articleComment)
    } catch (err) {
      res.status(500).send({
        error: 'An error has occured trying to fetch the articleComment.'
      })
    }
  },
  async post (req, res) {
    try {
      const userId = req.user.id
      const articleComment = await ArticleComment.create(req.body)
      console.log(userId)
      res.send(articleComment)
    } catch (err) {
      res.status(500).send({
        error: 'An error has occured trying to create the articleComment.'
      })
    }
  },
  async put (req, res) {
    try {
      await ArticleComment.update(req.body, {
        where: {
          id: req.params.id
        }
      })
      res.send(req.body)
    } catch (err) {
      res.status(500).send({
        error: 'an error has occured trying to update the articleComment'
      })
    }
  },
  async remove (req, res) {
    try {
      const {id} = req.params
      const articleComment = await ArticleComment.findOne({
        where: {
          id: id
        }
      })
      if (!articleComment) {
        return res.status(403).send({
          error: 'you do not have access to this articleComment'
        })
      }
      await articleComment.destroy()
      res.send(articleComment)
    } catch (err) {
      res.status(500).send({
        error: 'an error has occured trying to delete the articleComment'
      })
    }
  }
}
