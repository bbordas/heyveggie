const {Food} = require('../models')
module.exports = {
  async index (req, res) {
    try {
      let foods = null
      const search = req.query.search
      if (search) {
        foods = await Food.findAll({
          where: {
            $or: [
              'food_group_id', 'long_desc'
            ].map(key => ({
              [key]: {
                $like: `%${search}%`
              }
            }))
          }
        })
      } else {
        foods = await Food.findAll()
      }
      res.send(foods)
    } catch (err) {
      res.status(500).send({
        error: 'An error has occured trying to fetch foods.'
      })
    }
  },
  async show (req, res) {
    try {
      const food = await Food.findById(req.params.id)
      res.send(food)
    } catch (err) {
      res.status(500).send({
        error: 'An error has occured trying to fetch the food.'
      })
    }
  }
}
