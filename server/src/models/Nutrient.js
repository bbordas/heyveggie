module.exports = (sequelize, DataTypes) => {
  const Nutrient = sequelize.define('nutrient', {
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: false
  },
  units: DataTypes.STRING,
  tagname: DataTypes.STRING,
  name: DataTypes.STRING,
  num_decimal_places: DataTypes.STRING,
  sr_order: DataTypes.INTEGER,
},{
  timestamps: false,
  freezeTableName: true
})
  
return Nutrient
}