module.exports = (sequelize, DataTypes) => {
  const Recipe = sequelize.define('Recipe', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    title: DataTypes.STRING,
    description: DataTypes.TEXT,
    description2: DataTypes.TEXT,
    description3: DataTypes.TEXT,
    pictureURL: DataTypes.STRING,
    youtubeId: DataTypes.STRING,
    serves: DataTypes.INTEGER,
    chef: DataTypes.STRING,
    cuisine: DataTypes.STRING,
    step1: DataTypes.STRING,
    step2: DataTypes.STRING,
    step3: DataTypes.STRING,
    step4: DataTypes.STRING,
    step5: DataTypes.STRING,
    step6: DataTypes.STRING,
    step7: DataTypes.STRING,
    step8: DataTypes.STRING,
    step9: DataTypes.STRING,
    step10: DataTypes.STRING,
    step11: DataTypes.STRING,
    step12: DataTypes.STRING,
    approved: DataTypes.BOOLEAN,
    time: DataTypes.INTEGER
  })

  return Recipe
}