module.exports = (sequelize, DataTypes) => {
  const Cuisines = sequelize.define('Cuisines', {
  type: DataTypes.STRING,
},{
  timestamps: false,
})

  return Cuisines
}