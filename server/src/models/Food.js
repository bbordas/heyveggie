module.exports = (sequelize, DataTypes) => {
  const food = sequelize.define('food', {
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: false
  },
  food_group_id: DataTypes.INTEGER,
  long_desc: DataTypes.STRING,
  short_desc: DataTypes.STRING,
  common_names: DataTypes.STRING,
  manufac_name: DataTypes.STRING,
  survey: DataTypes.STRING,
  ref_desc: DataTypes.STRING,
  refuse: DataTypes.INTEGER,
  sci_name: DataTypes.STRING,
  nitrogen_factor: DataTypes.FLOAT,
  protein_factor: DataTypes.FLOAT,
  fat_factor: DataTypes.FLOAT,
  calorie_factor: DataTypes.FLOAT
},{
  timestamps: false,
  freezeTableName: true
})
  
return food
}