module.exports = (sequelize, DataTypes) => {
  const Weight = sequelize.define('weight', {
  food_id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: false
  },
  sequence_num: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: false
  },
  amount: DataTypes.FLOAT,
  description: DataTypes.STRING,
  gm_weight: DataTypes.FLOAT,
  num_data_pts: DataTypes.INTEGER,
  std_dev: DataTypes.FLOAT,
},{
  timestamps: false,
  freezeTableName: true
})
  
return Weight
}