module.exports = (sequelize, DataTypes) => {
  const CookBook = sequelize.define('CookBook', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    title: DataTypes.STRING
  })
  return CookBook
}