module.exports = (sequelize, DataTypes) => {
  const Nutrition = sequelize.define('nutrition', {
    food_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: false
    },
    nutrient_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: false
    },
    amount: DataTypes.FLOAT,
    num_data_points: DataTypes.INTEGER,
    std_error: DataTypes.STRING,
    source_code: DataTypes.STRING,
    derivation_code: DataTypes.STRING,
    reference_food_id: DataTypes.INTEGER,
    added_nutrient: DataTypes.STRING,
    num_studients: DataTypes.INTEGER,
    min: DataTypes.FLOAT,
    max: DataTypes.FLOAT,
    degrees_freedom: DataTypes.INTEGER,
    lower_error_bound: DataTypes.FLOAT,
    upper_error_bound:  DataTypes.FLOAT,
    comments:  DataTypes.STRING,
    modification_date:  DataTypes.STRING,
    confidence_code:  DataTypes.STRING,
},{
  timestamps: false,
  freezeTableName: true
})
  
return Nutrition
}