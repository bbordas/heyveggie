module.exports = (sequelize, DataTypes) => {
  const Ingredient = sequelize.define('Ingredient', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    food: DataTypes.STRING,
    food_id: DataTypes.INTEGER,
    amountPortion: DataTypes.INTEGER,
    labelPortion: DataTypes.STRING,
    weightInGrams: DataTypes.STRING,
    recipe_id: DataTypes.INTEGER
  },{
    timestamps: false,
  })

  return Ingredient
}