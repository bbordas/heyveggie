module.exports = (sequelize, DataTypes) => {
  const RecipeComment = sequelize.define('RecipeComment', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    text: DataTypes.TEXT,
    recipeId: DataTypes.INTEGER,
    email: DataTypes.STRING,
    author: DataTypes.STRING,
    title: DataTypes.STRING
  })
  return RecipeComment
}
