
module.exports = (sequelize, DataTypes) => {
  const FoodGroup = sequelize.define('food_group', {
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: false,
    unique: true
  },
  name: DataTypes.STRING
},{
  timestamps: false,
  freezeTableName: true
})
  
return FoodGroup
}