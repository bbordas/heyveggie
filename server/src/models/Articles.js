module.exports = (sequelize, DataTypes) => {
  const Article = sequelize.define('Article', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    title: DataTypes.STRING,
    picture1URL: DataTypes.STRING,
    picture2URL: DataTypes.STRING,
    smallText: DataTypes.STRING,
    articleText: DataTypes.TEXT,
    articleText2: DataTypes.TEXT,
    articleText3: DataTypes.TEXT,
    articleText4: DataTypes.TEXT,
    articleText5: DataTypes.TEXT,
    author: DataTypes.STRING,
    approved: DataTypes.BOOLEAN
  })
  return Article
}
