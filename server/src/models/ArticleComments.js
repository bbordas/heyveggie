module.exports = (sequelize, DataTypes) => {
  const ArticleComment = sequelize.define('ArticleComment', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    comment: DataTypes.TEXT,
    email: DataTypes.STRING,
    articleId: DataTypes.INTEGER,
    author: DataTypes.STRING,
    title: DataTypes.STRING
  })
  return ArticleComment
}
