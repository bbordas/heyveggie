const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const morgan = require('morgan')
const {sequelize} = require('./models')
const Sequelize = require('sequelize')
const config = require('./config/config')

const UserModel = require('./models/User')
const ArticleModel = require('./models/Articles')
const RecipeModel = require('./models/Recipes')
const IngredientsModel = require('./models/Ingredients')
const FoodGroupModel = require('./models/FoodGroup')
const FoodModel = require('./models/Food')
const NutrientModel = require('./models/Nutrient')
const NutritionModel = require('./models/Nutrition')
const WeightModel = require('./models/Weight')
const CuisinesModel = require('./models/Cuisines')
const ArticleCommentModel = require('./models/ArticleComments')
const RecipeCommentModel = require ('./models/RecipeComments')

const app = express()
app.use(morgan('combined'))
app.use(bodyParser.json())
app.use(cors())

const User = UserModel(sequelize, Sequelize)
const Article = ArticleModel(sequelize, Sequelize)
const Recipe = RecipeModel(sequelize, Sequelize)
const Ingredients = IngredientsModel(sequelize, Sequelize)
const FoodGroup = FoodGroupModel(sequelize, Sequelize)
const Food = FoodModel(sequelize, Sequelize)
const Nutrient = NutrientModel(sequelize, Sequelize)
const Nutrition = NutritionModel(sequelize, Sequelize)
const Weight = WeightModel(sequelize, Sequelize)
const Cuisines = CuisinesModel(sequelize, Sequelize)
const ArticleComment = ArticleCommentModel(sequelize, Sequelize)
const RecipeComment = RecipeCommentModel(sequelize, Sequelize)

require('./passport')
require('./routes')(app)

sequelize.sync()
  .then(() => {
    app.listen(config.port)
    console.log(`Server started on ${config.port}`)
  })

Article.belongsTo(User,  {foreignKey: 'author', targetKey: 'email'})
Recipe.belongsTo(User, {foreignKey: 'chef', targetKey: 'email'})
Ingredients.belongsTo(Recipe, {foreignKey:'recipe_id', targetKey: 'id'})
Cuisines.belongsTo(Recipe, {foreignKey: 'cuisine_id', targetKey: 'id'})
Food.belongsTo(Ingredients, {foreignKey: 'food_id', targetKey: 'id'})
RecipeComment.belongsTo(Recipe, {foreignKey: 'recipeId', targetKey: 'id'})
RecipeComment.belongsTo(User, {foreignKey: 'email', targetKey: 'email'})
ArticleComment.belongsTo(User, {foreignKey: 'email', targetKey: 'email'})
ArticleComment.belongsTo(Article, {foreignKey: 'articleId', targetKey: 'id'})

module.exports = {
  User,
  Article,
  Recipe,
  Ingredients,
  Food,
  ArticleComment,
  RecipeComment
}