const { 
  sequelize,
  Recipe,
  Article,
  User,
  CookBook,
  Cuisines,
  Ingredients
} = require('../src/models')

const Promise = require('bluebird')
const recipes = require('./recipes.json')
const users = require('./users.json')
const articles = require('./articles.json')
const cookbooks = require('./cookbook.json')
const cuisines = require('./cuisines.json')
const ingredients = require('./ingredients.json')

sequelize.sync({force:true})
  .then(async function () {
  await Promise.all(
    users.map(user => {
       User.create(user)
     })
  )

   await Promise.all(
      recipes.map(recipe => {
        Recipe.create(recipe)
      })
     )

     await Promise.all(
      articles.map(article => {
        Article.create(article)
      })
     )

     await Promise.all(
      cookbooks.map(cookbook => {
        CookBook.create(cookbook)
      })
     )

     await Promise.all(
      cuisines.map(cuisines => {
        Cuisines.create(cuisines)
      })
     )

     await Promise.all(
       ingredients.map(ingredients => {
         Ingredients.create(ingredients)
       })
     ) 
  })