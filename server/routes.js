const AuthenticationController = require('./controllers/AuthenticationController')
const AuthenticationControllerPolicy = require('./policies/AuthenticationControllerPolicy')
const RecipesController = require('./controllers/RecipesController')
const ArticlesController = require('./controllers/ArticlesController')
const UsersController = require('./controllers/UsersController')
const CookbooksController = require('./controllers/CookbooksController')
const isAuthenticated = require('./policies/isAuthenticated')

module.exports = (app) => {
  app.post('/register',
    AuthenticationControllerPolicy.register, 
    AuthenticationController.register) 
  app.post('/login',
    AuthenticationController.login)
  app.get('/users',
    isAuthenticated,
    UsersController.index)
  app.get('/users/:userId',
    isAuthenticated,
    UsersController.show)

  app.get('/recipes',
    RecipesController.index)
  app.get('/recipes/:recipeId',
    RecipesController.show)
  app.post('/recipes',
    isAuthenticated,
    RecipesController.post)
  app.put('/recipes/:recipeId',
    isAuthenticated,
    RecipesController.put)
  app.delete('/recipes/:recipeId',
    isAuthenticated,  
    RecipesController.remove)

  app.get('/articles',
    ArticlesController.index)
  app.get('/articles/:articleId',
    ArticlesController.show)
  app.post('/articles',
    isAuthenticated,
    ArticlesController.post)
  app.put('/articles/:articleId',
    isAuthenticated,
    ArticlesController.put)   
  app.delete('/articles/:articleId',
    isAuthenticated,  
    ArticlesController.remove)

    

  app.get('/cookbooks',
    isAuthenticated,
    CookbooksController.index)

 

  

  

  

  

  
}
