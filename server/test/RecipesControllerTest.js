var should = require('should')
var sinon = require('sinon')
var recipeController = require('../src/controllers/RecipesController'){Recipe}

describe('Recipe Controller Test', function () {
  describe('Post', function() {
    it('it should not allow an empty title on post', function () {
      var Recipe = function(recipe){this.create = function(){}}

      var req = {
        body: {
          title: 'Vegan Cauliflower wings',
          description: 'These vegan cauliflower hot wings with vegan aioli are the perfect comfort food! It’s almost a bit creepy how close they are to the real thing! They’re so tangy, spicy, and incredibly comforting. And they’re also a lot healthier than chicken wings! Oh, and they’re just in time for football season! I’ve wanted to try vegan cauliflower wings for sooo long. But somehow I never got around to actually making them. There are just waaay too many vegan things to try. And some people say vegan food is boring and restrictive. Well, not really! On the contrary, I’m always amazed what you can do with food. Like making vegan jackfruit pulled pork or vegan cheese sauce with cashews. I didn’t really like chefing before I became vegan or at least I wasn’t really into it. But when I became vegan, I was so eager to try all these new things and I absolutely fell in love with chefing and food! It’s one of my greatest passions.',
          cuisine: 'American',
          pictureURL: 'https://www.rawtillwhenever.com/wp-content/uploads/2017/01/cauliflower-buffalo-wings-vegan-4.jpg',
          youtubeId: 'kvL8jZo6KHc',
          serves: 4,
          calories: 560,
          chef: 'testchef@gmail.com',
          approved: true
        }
      }
      var res = {
        status: sinon.spy(),
        send: sinon.spy()
      }

      recipeController.post(req, res)

      res.status.calledWith(400).should.equal(true, 'Bad Status' + res.status.args[0][0])
      res.send.calledWith('Title is required').should.equal(false)
    })
  })
})