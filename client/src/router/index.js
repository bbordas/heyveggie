import Vue from 'vue'
import Router from 'vue-router'
import Index from '@/components/Index'

import Register from '@/components/Register'
import Login from '@/components/Login'

import Recipes from '@/components/Recipes/Index'
import Articles from '@/components/Articles/Index'
import Messages from '@/components/Messages/Index'
import Admin from '@/components/Admin/Index'
// import Users from '@/components/User/Index'

import CreateRecipe from '@/components/CreateRecipe'
import CreateArticles from '@/components/CreateArticles'

import ViewUser from '@/components/ViewUser/Index'
import ViewRecipe from '@/components/ViewRecipe/Index'
import ViewArticle from '@/components/ViewArticle/Index'
import ViewCookBook from '@/components/CookBook/Index'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/index',
      name: 'index',
      component: Index
    },
    {
      path: '/register',
      name: 'register',
      component: Register
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    /*     {
      path: '/users',
      name: 'users',
      component: Users
    },
    {
      path: '/users/:userId',
      name: 'user',
      component: Users
    }, */
    {
      path: '/profile',
      name: 'profile',
      component: ViewUser
    },
    {
      path: '/admin',
      name: 'admin',
      component: Admin
    },
    {
      path: '/articles',
      name: 'articles',
      component: Articles
    },
    {
      path: '/articles/create',
      name: 'articles-create',
      component: CreateArticles
    },
    {
      path: '/articles/:articleId',
      name: 'article',
      component: ViewArticle
    },
    {
      path: '/recipes',
      name: 'recipes',
      component: Recipes
    },
    {
      path: '/recipes/create',
      name: 'recipes-create',
      component: CreateRecipe
    },
    {
      path: '/recipes/:recipeId',
      name: 'recipe',
      component: ViewRecipe
    },
    {
      path: '/cookbook/:cookbookId',
      name: 'cookbook',
      component: ViewCookBook
    },
    {
      path: '/messages',
      name: 'messages',
      component: Messages
    },
    {
      path: '*',
      redirect: 'index'
    }
  ]
})
