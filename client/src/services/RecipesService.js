import Api from '@/services/Api'

export default {
  index (search) {
    return Api().get('recipes', {
      params: {
        search: search
      }
    })
  },
  show (recipeId) {
    return Api().get(`recipes/${recipeId}`)
  },
  post (recipe) {
    return Api().post('recipes', recipe)
  },
  put (recipe) {
    return Api().put(`recipes/${recipe.id}`, recipe)
  },
  delete (recipeId) {
    return Api().delete(`recipes/${recipeId}`)
  }
}
