import Api from '@/services/Api'

export default {
  index (search) {
    return Api().get('articlecomments', {
      params: {
        search: search
      }
    })
  },
  show (articlecommentId) {
    return Api().get(`articlecomments/${articlecommentId}`)
  },
  post (articlecomment) {
    return Api().post('articlecomments', articlecomment)
  },
  put (articlecomment) {
    return Api().put(`articlecomments/${articlecomment.id}`, articlecomment)
  },
  delete (articlecommentId) {
    return Api().delete(`articlecomments/${articlecommentId}`)
  }
}
