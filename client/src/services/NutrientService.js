import Api from '@/services/Api'

export default {
  index (search) {
    return Api().get('nutrient', {
      params: {
        search: search
      }
    })
  },
  show (nutrientId) {
    return Api().get(`nutrient/${nutrientId}`)
  },
  post (nutrient) {
    return Api().post('nutrient', nutrient)
  },
  put (nutrient) {
    return Api().put(`nutrient/${nutrient.id}`, nutrient)
  },
  delete (nutrientId) {
    return Api().delete(`nutrient/${nutrientId}`)
  }
}
