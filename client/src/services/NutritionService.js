import Api from '@/services/Api'

export default {
  index (search) {
    return Api().get('nutrition', {
      params: {
        search: search
      }
    })
  },
  show (nutritionId) {
    return Api().get(`nutrition/${nutritionId}`)
  },
  post (nutrition) {
    return Api().post('nutrition', nutrition)
  },
  put (nutrition) {
    return Api().put(`nutrition/${nutrition.id}`, nutrition)
  },
  delete (nutritionId) {
    return Api().delete(`nutrition/${nutritionId}`)
  }
}
