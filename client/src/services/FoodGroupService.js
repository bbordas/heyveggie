import Api from '@/services/Api'

export default {
  index (search) {
    return Api().get('foodgroup', {
      params: {
        search: search
      }
    })
  },
  show (foodgroupId) {
    return Api().get(`foodgroup/${foodgroupId}`)
  },
  post (foodgroup) {
    return Api().post('foodgroup', foodgroup)
  },
  put (foodgroup) {
    return Api().put(`foodgroup/${foodgroup.id}`, foodgroup)
  },
  delete (foodgroupId) {
    return Api().delete(`foodgroup/${foodgroupId}`)
  }
}
