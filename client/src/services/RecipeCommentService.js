import Api from '@/services/Api'

export default {
  index (search) {
    return Api().get('recipecomments', {
      params: {
        search: search
      }
    })
  },
  show (recipecommentId) {
    return Api().get(`recipecomments/${recipecommentId}`)
  },
  post (recipecomment) {
    return Api().post('recipecomments', recipecomment)
  },
  put (recipecomment) {
    return Api().put(`recipecomments/${recipecomment.id}`, recipecomment)
  },
  delete (recipecommentId) {
    return Api().delete(`recipecomments/${recipecommentId}`)
  }
}
