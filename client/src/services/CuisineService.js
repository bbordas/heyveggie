import Api from '@/services/Api'

export default {
  index (search) {
    return Api().get('cuisine', {
      params: {
        search: search
      }
    })
  },
  show (cuisineId) {
    return Api().get(`cuisine/${cuisineId}`)
  },
  post (cuisine) {
    return Api().post('cuisine', cuisine)
  },
  put (cuisine) {
    return Api().put(`cuisine/${cuisine.id}`, cuisine)
  },
  delete (cuisineId) {
    return Api().delete(`cuisine/${cuisineId}`)
  }
}
