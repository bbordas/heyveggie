import Api from '@/services/Api'

export default {
  index (search) {
    return Api().get('cookbooks', {
      params: {
        search: search
      }
    })
  },
  show (cookbookId) {
    return Api().get(`cookbooks/${cookbookId}`)
  },
  post (cookbook) {
    return Api().post('cookbooks', cookbook)
  },
  put (cookbook) {
    return Api().put(`cookbooks/${cookbook.id}`, cookbook)
  },
  delete (cookbookId) {
    return Api().delete(`cookbooks/${cookbookId}`)
  }
}
