import Api from '@/services/Api'

export default {
  index (search) {
    return Api().get('weight', {
      params: {
        search: search
      }
    })
  },
  show (weightId) {
    return Api().get(`weight/${weightId}`)
  },
  post (weight) {
    return Api().post('weight', weight)
  },
  put (weight) {
    return Api().put(`weight/${weight.id}`, weight)
  },
  delete (weightId) {
    return Api().delete(`weight/${weightId}`)
  }
}
