import Api from '@/services/Api'

export default {
  index (search) {
    return Api().get('ingredients', {
      params: {
        search: search
      }
    })
  },
  show (ingredientId) {
    return Api().get(`ingredients/${ingredientId}`)
  },
  post (ingredients) {
    return Api().post('ingredients', ingredients)
  },
  put (ingredients) {
    return Api().put(`ingredients/${ingredients.id}`, ingredients)
  }
}
