import Api from '@/services/Api'

export default {
  index (search) {
    return Api().get('food', {
      params: {
        search: search
      }
    })
  },
  show (foodId) {
    return Api().get(`food/${foodId}`)
  }
}
